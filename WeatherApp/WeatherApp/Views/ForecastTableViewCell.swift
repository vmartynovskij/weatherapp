//
//  ForecastTableViewCell.swift
//  WeatherApp
//
//  Created by Viktor Martynovskij on 17.08.2021.
//

import UIKit

class ForecastTableViewCell: UITableViewCell {
   
    @IBOutlet weak var mainImageView: UIImageView!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var mainLabel: UILabel!
    @IBOutlet weak var tempLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
