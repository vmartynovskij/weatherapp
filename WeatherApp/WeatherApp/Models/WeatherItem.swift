//
//  WeatherItem.swift
//  WeatherApp
//
//  Created by Viktor Martynovskij on 17.08.2021.
//

import Foundation
import SwiftyJSON

struct WeatherItem: Codable {
    var dt_txt = "" //"2021-08-17 12:00:00"
    var icon = "" //"01d"
    var main = "" //"Clear"
    var temp = 0 //32.369999999999997
    var wind_deg = 0 // 159
    var wind_speed = 0 //3.31
    var humidity = 0
    var pressure = 0
    
    init (json: JSON){
        
        self.dt_txt = json["dt_txt"].string ?? ""
        self.icon = json["weather"][0]["icon"].string ?? ""
        self.main = json["weather"][0]["main"].string ?? ""
        self.temp = json["main"]["temp"].int ?? 0
        self.wind_deg = json["wind"]["deg"].int ?? 0
        self.wind_speed = json["wind"]["speed"].int ?? 0
        self.pressure = json["main"]["pressure"].int ?? 0
        self.humidity = json["main"]["humidity"].int ?? 0
    }
    
    static func array(jsons: [JSON]) -> [WeatherItem]{
        var array = [WeatherItem]()
        for json in jsons{
            array.append(WeatherItem(json: json))
        }
        return array
    }
}
