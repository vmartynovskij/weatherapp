//
//  ForecastViewController.swift
//  WeatherApp
//
//  Created by Viktor Martynovskij on 17.08.2021.
//

import UIKit

class ForecastViewController: BaseViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var tableView: UITableView!
    var orderedList:[TimeInterval:[WeatherItem]] = [TimeInterval:[WeatherItem]]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
   
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.navigationItem.title = Global.shared.city
        
        if (Global.shared.list.count > 0){
            for i in 0...Global.shared.list.count-1{
                let item = Global.shared.list[i]
                let dateStr:String = item.dt_txt.components(separatedBy: " ").first!
                
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "yyyy-MM-dd"
                let date = dateFormatter.date(from:dateStr)!
                let dateTimeInterval = (date.timeIntervalSince1970)
                
                if (self.orderedList[dateTimeInterval] != nil){
                    var array:[WeatherItem] = self.orderedList[dateTimeInterval]!
                    array.append(item)
                    self.orderedList[dateTimeInterval] = array
                } else {
                    self.orderedList[dateTimeInterval] = [item]
                }
            }
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.orderedList.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        //        return Global.shared.list.count
        return self.orderedList[Array(self.orderedList.keys.sorted())[section]]!.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 30.0
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let view:UIView = UIView.init(frame: CGRect.init(x: 0, y: 0, width: 300, height: 30))
        view.backgroundColor = UIColor.lightGray
        
        if (self.orderedList.count == 0) {
            return view
        }
        
        
        let dateLabel = UILabel.init(frame: CGRect(x: 30, y: 5, width: 200, height: 20))
        dateLabel.font = UIFont.boldSystemFont(ofSize: 18)
        dateLabel.textColor = UIColor.black

        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        let date = Date.init(timeIntervalSince1970: Array(self.orderedList.keys.sorted())[section])

        if (Calendar.current.isDateInToday(date)){
            dateLabel.text = NSLocalizedString("TODAY", tableName: "", comment: "")
        } else if (Calendar.current.isDateInYesterday(date)){
            dateLabel.text = NSLocalizedString("YESTERDAY", tableName: "", comment: "")
        } else {
            dateFormatter.dateFormat = "d MMMM"
            dateLabel.text = dateFormatter.string(from: date).uppercased()
        }
        
        view.addSubview(dateLabel)
        return view
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ForecastTableViewCell") as! ForecastTableViewCell
        
        let wItem = self.orderedList[Array(self.orderedList.keys.sorted())[indexPath.section]]![indexPath.row]
        cell.mainImageView.kf.setImage(with: URL(string: "https://api.openweathermap.org/img/w/\(wItem.icon).png"))
        cell.timeLabel.text = wItem.dt_txt.components(separatedBy: " ").last
        cell.mainLabel.text = wItem.main
        cell.tempLabel.text = "\(wItem.temp)℃"
        
            return cell
    }

}
