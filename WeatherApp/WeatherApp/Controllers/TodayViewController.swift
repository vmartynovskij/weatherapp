//
//  TodayViewController.swift
//  WeatherApp
//
//  Created by Viktor Martynovskij on 17.08.2021.
//

import UIKit
import Kingfisher
import CoreLocation

class TodayViewController: BaseViewController {
    @IBOutlet weak var mainImageView: UIImageView!
    @IBOutlet weak var cityLabel: UILabel!
    @IBOutlet weak var tempLabel: UILabel!
    
    @IBOutlet weak var humidityImgView: UIImageView!
    @IBOutlet weak var humidityLabel: UILabel!
    @IBOutlet weak var pressureLabel: UILabel!
    @IBOutlet weak var windSpeedLabel: UILabel!
    @IBOutlet weak var windDirectionLabel: UILabel!
    
    @IBOutlet weak var winDirectionImgView: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        self.navigationItem.title = "Today"

        NotificationCenter.default.addObserver(self, selector: #selector(self.locationUpdated(notification:)), name: Notification.Name("locationUpdated"), object: nil)
        
        if CLLocationManager.locationServicesEnabled() {
            switch CLLocationManager.authorizationStatus() {
            case .restricted, .denied:
                print("No access")
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.0) {
                    let alertController = UIAlertController (title: "Error", message: "Please allow locations services in device settings.", preferredStyle: .alert)
                    
                    let settingsAction = UIAlertAction(title: "Settings", style: .default) { (_) -> Void in
                        
                        guard let settingsUrl = URL(string: UIApplication.openSettingsURLString) else {
                            return
                        }
                        
                        if UIApplication.shared.canOpenURL(settingsUrl) {
                            UIApplication.shared.open(settingsUrl, completionHandler: { (success) in
                                print("Settings opened: \(success)") // Prints true
                            })
                        }
                    }
                    alertController.addAction(settingsAction)
                    let cancelAction = UIAlertAction(title: "Cancel", style: .default, handler: nil)
                    alertController.addAction(cancelAction)
                    
                    self.present(alertController, animated: true, completion: nil)
                }
                
            break
                case .authorizedAlways, .authorizedWhenInUse:
                    print("Access")
            case .notDetermined:
                break
            @unknown default:
                break
            }
            } else {
                print("Location services are not enabled")
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if (Global.shared.list.count > 0){
            locationUpdated(notification: Notification.init(name: Notification.Name(rawValue: "")))
        }
    }
    

    @objc func locationUpdated(notification: Notification) {
        
        print("locationUpdated")
        
        let wItem = Global.shared.list.first
        mainImageView.kf.setImage(with: URL(string: "https://api.openweathermap.org/img/w/\(wItem?.icon ?? "10n").png"), placeholder: nil)
        cityLabel.text = Global.shared.city
        tempLabel.text = "\(wItem?.temp ?? 0) ℃"
        
        humidityLabel.text = "\(wItem?.humidity ?? 0)%"
        pressureLabel.text = "\(wItem?.pressure ?? 0) hPa"
        windSpeedLabel.text = "\(wItem?.wind_speed ?? 0) km/h"
        

        
        
//        winDirectionImgView.transform = winDirectionImgView.transform.rotated(by: ((wItem?.wind_deg ?? 0) * .pi / 180))
//        winDirectionImgView.transform = CGAffineTransform(rotationAngle: CGFloat.pi/2)
        if (wItem != nil){
            winDirectionImgView.transform = CGAffineTransform(rotationAngle: (CGFloat(wItem!.wind_deg) * .pi / 180))
            
            var winD = "N"
            if (wItem!.wind_deg > (45-22) && wItem!.wind_deg < (45+22)){
                winD = "NE"
            } else if (wItem!.wind_deg > (90-22) && wItem!.wind_deg < (90+22)){
                winD = "E"
            } else if (wItem!.wind_deg > (135-22) && wItem!.wind_deg < (135+22)){
                winD = "SE"
            } else if (wItem!.wind_deg > (180-22) && wItem!.wind_deg < (180+22)){
                winD = "S"
            } else if (wItem!.wind_deg > (225-22) && wItem!.wind_deg < (225+22)){
                winD = "SW"
            } else if (wItem!.wind_deg > (270-22) && wItem!.wind_deg < (270+22)){
                winD = "W"
            } else if (wItem!.wind_deg > (315-22) && wItem!.wind_deg < (315+22)){
                winD = "NW"
            }
            windDirectionLabel.text = "\(wItem?.wind_deg ?? 0)° - \(winD)"
        }
    }
    
    @IBAction func shareAction(_ sender: UIButton) {
        let wItem = Global.shared.list.first
        
        sender.isHidden = true
        let shareAll = ["\(Global.shared.city), \(wItem?.temp ?? 0) ℃, Humidity:\(wItem?.humidity ?? 0)%, Pressure:\(wItem?.pressure ?? 0) hPa, Wind speed:\(wItem?.wind_speed ?? 0) km/h, Wind direction:\(wItem?.wind_deg ?? 0)°" , view.asImage()] as [Any]
        sender.isHidden = false
        let activityViewController = UIActivityViewController(activityItems: shareAll, applicationActivities: nil)
        activityViewController.popoverPresentationController?.sourceView = self.view
        self.present(activityViewController, animated: true, completion: nil)
    }
    
}
