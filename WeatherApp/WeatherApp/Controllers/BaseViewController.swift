//
//  BaseViewController.swift
//  WeatherApp
//
//  Created by Viktor Martynovskij on 17.08.2021.
//

import UIKit

class BaseViewController: UIViewController
{
    func showMessage(title: String? = "", message: String? = ""){
        let alertController = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
        
        alertController.addAction(UIAlertAction(title: "OK", style: .default, handler: { (action) in
            alertController.dismiss(animated: true, completion: nil)
        }))
        self.present(alertController, animated: true)
    }
}
