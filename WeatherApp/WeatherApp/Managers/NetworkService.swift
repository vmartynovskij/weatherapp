//
//  NetworkService.swift
//  WeatherApp
//
//  Created by Viktor Martynovskij on 17.08.2021.
//

import Foundation
import Alamofire
import SwiftyJSON
import SVProgressHUD


class NetworkService: NSObject {
    
    let API_BASE_URL = "https://community-open-weather-map.p.rapidapi.com/"
    let headers = [
        "x-rapidapi-key": "2b867d64bemsh5fa6ea2606667b5p19e48fjsn82c869691ec9",
        "x-rapidapi-host": "community-open-weather-map.p.rapidapi.com"
    ]
    
    func forecast(location:String, completion: @escaping (JSON, Bool)->Void){
        let url = API_BASE_URL + "forecast?q=\(location)&units=metric"
        
        Alamofire.upload(multipartFormData: { (formData) in
            self.append(formData: formData, with: nil)
            
        }, to: url, method:.get, headers: headers, encodingCompletion: { (result) in
            NetworkService.completeFormData(result: result, completion: { (json, isComplete) in
                completion(json, isComplete)
            })
        })
    }
    
    private func append(formData: MultipartFormData, with parameters: Parameters?, additionalParams: [[String : Any]]? = nil){
        print(parameters as Any)
        if let parameters = parameters{
            for (key, value) in parameters {
                var data = ("\(value)" as AnyObject).data(using: String.Encoding.utf8.rawValue)!
                if value is String {
                    data = (value as! String).data(using: .utf8)!
                }
                formData.append(data, withName: key)
            }
        }
        
        if (additionalParams != nil){
            print(additionalParams as Any)
            for (additionalParam) in additionalParams! {
                    for (key, value) in additionalParam {
                        var data = ("\(value)" as AnyObject).data(using: String.Encoding.utf8.rawValue)!
                        if value is String {
                            data = (value as! String).data(using: .utf8)!
                        }
                        formData.append(data, withName: key)
                    }
            }
        }
    }
    
    static func completeFormData (result : SessionManager.MultipartFormDataEncodingResult, completion: @escaping (JSON, Bool)->Void)
    {
        switch result{
        case .success(let upload, _, _):
            upload.uploadProgress(closure: {(progress) in
                
            })
            upload.validate()
            upload.responseJSON{ response in
                
//                SVProgressHUD.dismiss()
                
                print(response.request!.httpMethod! + " : " + response.request!.url!.absoluteString)
                
                if let result = response.result.value {

                    let serverResponse = result
                    let data = JSON(serverResponse)
                    if response.response?.statusCode == 200 {
                        completion(data, true)
                    } else {
                        completion(data, false)
                    }
                }else{
                    if response.response?.statusCode == 200 {
                        completion(JSON(""), true)
                    } else {
                        if let data = response.data {
                            let data = JSON(data)
                            print(data)
                            completion(data, false)
                        }
                        else {
                            completion(JSON(""), false)
                        }
                    }
                }
            }
            break
        case .failure(let error):
            print(error)
            completion(JSON(""), false)
            break
        }
    }
}
