//
//  Global.swift
//  WeatherApp
//
//  Created by Viktor Martynovskij on 17.08.2021.
//

import Foundation
import CoreLocation
import UIKit
import Firebase
//import FirebaseDatabase

class Global: NSObject, CLLocationManagerDelegate {
    static let shared = Global()
    
    let locationManager = CLLocationManager()
    var location : CLLocation? = nil
    var list:[WeatherItem] = []
    var city = ""
    
    func requestLocation(){
        
        if let savedCity = UserDefaults.standard.value(forKey: "city"){
            self.city = savedCity as! String
        }
        if let data = UserDefaults.standard.data(forKey: "list") {
            do {
                let decoder = JSONDecoder()
                self.list = try decoder.decode([WeatherItem].self, from: data)
            } catch {
                print("Unable to Decode Note (\(error))")
            }
        }
        
        // Ask for Authorisation from the User.
        self.locationManager.requestAlwaysAuthorization()

//        // For use in foreground
        self.locationManager.requestWhenInUseAuthorization()

        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
            
            locationManager.startUpdatingLocation()
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let locValue: CLLocationCoordinate2D = manager.location?.coordinate else { return }
        print("locations = \(locValue.latitude) \(locValue.longitude)")
        
        if (location == nil){
            location = Global.shared.locationManager.location
            
            UserDefaults.standard.set(["en"], forKey: "AppleLanguages")
            let geocoder = CLGeocoder()

            geocoder.reverseGeocodeLocation(location!, preferredLocale: Locale.init(identifier: "en_US"), completionHandler: { (placemarks, error) in
                if error == nil {
                    if let firstLocation = placemarks?[0]{
                        let city:String? = firstLocation.locality;
                        let state = firstLocation.administrativeArea;
                        let country = firstLocation.country;

                        NetworkService.init().forecast(location:"\(city ?? (state ?? "")),\(country ?? "")") { json, isComplete in
                            if (isComplete){
                                print(json)
                                
                                self.city = "\(json["city"]["name"].stringValue), \(json["city"]["country"].stringValue)"
                                self.list = WeatherItem.array(jsons: json["list"].arrayValue)
                                
                                if (self.list.count > 0){
                                    do {
                                        let encoder = JSONEncoder()
                                        let data = try encoder.encode(self.list)
                                        UserDefaults.standard.set(data, forKey: "list")
                                        UserDefaults.standard.set(self.city, forKey: "city")
                                        
                                        let rootRef = Database.database().reference()
                                        rootRef.child("users").child(UIDevice.current.identifierForVendor!.uuidString).setValue(["uuid": UIDevice.current.identifierForVendor!.uuidString, "city":self.city, "temp":"\(self.list.first?.temp ?? 0)", "time":self.list.first?.dt_txt])
                                        
                                    } catch {
                                        print("Unable to Encode Note (\(error))")
                                    }
                                    
                                    
                                    
                                    NotificationCenter.default.post(name: Notification.Name("locationUpdated"), object: nil)
                                }
                            } else {
                                (UIApplication.topViewController() as! BaseViewController).showMessage(title: "Error", message: "Error while fetching data from rapidapi.com")
                            }
                        }
                        
                    }
                }
            })
        }
        
        
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error)
    {
        print("Errors: " + error.localizedDescription)
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if status == .authorizedAlways {
            if CLLocationManager.isMonitoringAvailable(for: CLBeaconRegion.self) {
                if CLLocationManager.isRangingAvailable() {
                    // do stuff
                }
            }
        }
        if status == .denied {
            // handle your case
        }
    }
}
